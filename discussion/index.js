const http = require ("http");

const port = 4000;

const server = http.createServer((req, res) => {
	//http method of the incoming request can be accessed via "req.method"
		//GET method - retrieving/reading information; default method
/*
	POST, PUT, and DELETE operation do not work in the browser unlike the GET method. With this, Postman solves the problem by simulating a frontend for the developes to test their codes.
*/

	if (req.url === "/items" && req.method === "GET"){
		res.writeHead(200,{"Content-Type":"text/plain"});
		res.end("Data retrieved from the database");}


	if (req.url === "/items" && req.method === "POST"){
		res.writeHead(200,{"Content-Type":"text/plain"});
		res.end("Data to be sent to the database");
	}	

	if (req.url === "/items" && req.method === "PUT"){
		res.writeHead(200,{"Content-Type":"text/plain"});
		res.end("Update resources");
	}

	if (req.url === "/items" && req.method === "DELETE"){
		res.writeHead(200,{"Content-Type":"text/plain"});
		res.end("Delete resources");
	}	
	
});

/*
	mini activity
	- create "items" url with POST method
	the response should be "Data to be sent to the database" with 200 as a status code and plain text as the content type"
*/

	
server.listen(port);

console.log(`Server is running at localhost: ${port}`);



